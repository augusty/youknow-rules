# -*- coding: utf-8 -*-

#
# 作者：ysjlion
# 日期：20220424
# 备注：geoip离线库放在与该py文件同一目录下
#

import os
import time
import sys
import requests
import re
import shutil
import json
import geoip2.database
# pip install geoip2 离线文件下载：https://dev.maxmind.com/geoip/geoip2/geolite2/
# pip install requests[socks]

def get_server_clash(servers_url):
  server = ''
  # 根据网络状况设置代理
  my_proxy={"http":"socks5://127.0.0.1:7891","https":"socks5://127.0.0.1:7891"}
  for server_url in servers_url:
    print('Loading: ' + server_url)
    # 获取网页内容
    success = False
    try_times = 0
    r = None
    # 识别关键字走代理
    keywd='github.com|githubusercontent.com'
    # 全部都代理或都不走代理
    #keywd='111111'
    while try_times < 5 and not success:
      if re.search(keywd, server_url):
        r = requests.get(server_url, proxies=my_proxy)
      else:
        r = requests.get(server_url)
      if r.status_code != 200:
        time.sleep(1)
        try_times = try_times + 1
      else:
        success = True
        break

    if not success:
      sys.exit('error in request %s\n\treturn code: %d' % (server_url, r.status_code) )

    server = server + r.text + '\n'

  return server

def get_server_v2rayN(servers_url):
  server = []
  # 根据网络状况设置代理
  my_proxy={"http":"socks5://127.0.0.1:7891","https":"socks5://127.0.0.1:7891"}
  for server_url in servers_url:
    print('Loading: ' + server_url)
    # 获取网页内容
    success = False
    try_times = 0
    r = None
    # 识别关键字走代理
    keywd='github.com|githubusercontent.com'
    # 全部都代理或都不走代理
    #keywd='http'
    while try_times < 5 and not success:
      if re.search(keywd, server_url):
        r = requests.get(server_url, proxies=my_proxy)
      else:
        r = requests.get(server_url)
      if r.status_code != 200:
        time.sleep(1)
        try_times = try_times + 1
      else:
        success = True
        break

    if not success:
      sys.exit('error in request %s\n\treturn code: %d' % (server_url, r.status_code) )

    jsondata = r.json()
    list1 = jsondata['vmess'][0:] #选择从第0个元素到数组结尾的所有元素，返回一个列表
    server = server + list1
    list1.clear()

  return server

def removeduplicate_clash(list1):
    newlist = []
    for row in list1:
        rowkey = re.findall(r'(\bserver.*type\b)', row)
        mark = True
        #print('key= '+str(rowkey))
        for newrow in newlist:
            newrowkey = re.findall(r'(\bserver.*type\b)', newrow)
            if newrowkey == rowkey:
                mark = False
                break
        if mark:
            newlist.append(row)
            #print(row)
    return newlist

def removeduplicate_v2rayN(list1):
    """
    列表套字典去重复
    :param list1: 输入一个有重复值的列表
    :return: 返回一个去掉重复的列表
    """
    immutable_dict = set([str(item) for item in list1])  #遍历字典，将每个子项变成字符串存放到数组中，再通过set函数去重
    list1 = [eval(i) for i in immutable_dict]  #通过eval函数，将去重后的数组里的每个子项重新转化回字典
    return list1

def genV2rayN():

  servers_url = [
    'https://raw.githubusercontent.com/Alvin9999/PAC/master/1/guiNConfig.json',
    'https://raw.githubusercontent.com/Alvin9999/PAC/master/guiNConfig.json',
    'https://raw.githubusercontent.com/Alvin9999/PAC/master/3/guiNConfig.json',
    'https://raw.githubusercontent.com/Alvin9999/PAC/master/2/guiNConfig.json',
  ]

  # 获取网页内容
  print('>>>>       Getting v2rayN servers list...      <<<<')
  points = get_server_v2rayN(servers_url)  #网络读取适用
  #jsondata = json.load(open('temp.json', 'r', encoding='utf-8'))  #本地文件适用
  #points = jsondata['vmess'][0:]  #本地文件适用

  points = removeduplicate_v2rayN(points)  #去重

  try:
    if sys.version_info.major == 3:
      file_points = open('Output/v2rayN-servers.tmp', 'w', encoding='utf-8')
      file_pointsfss = open('Output/v2rayN-ss.txt', 'w', encoding='utf-8')
      file_pointsfv1 = open('Output/v2rayN-vmess(wss).txt', 'w', encoding='utf-8')
      file_pointsfv2 = open('Output/v2rayN-vmess.txt', 'w', encoding='utf-8')
      file_pointsftj = open('Output/v2rayN-trojan.txt', 'w', encoding='utf-8')
    else:
      file_points = open('Output/v2rayN-servers.tmp', 'w')
      file_pointsfss = open('Output/v2rayN-ss.txt', 'w')
      file_pointsfv1 = open('Output/v2rayN-vmess(wss).txt', 'w')
      file_pointsfv2 = open('Output/v2rayN-vmess.txt', 'w')
      file_pointsftj = open('Output/v2rayN-trojan.txt', 'w')
  except:
    pass

  file_points.write('# v2rayN Servers tempfile refresh time: ' + time.strftime("%Y-%m-%d %H:%M:%S") + '\n\n')

  pointsss = []
  pointsvm1 = []
  pointsvm2 = []
  pointstj = []

  # 从1开始编号，服务器数量一般不超过10个，如果超过需要修改Clash起始编号，否则服务器名称冲突。
  i = 1
  j1 = 1
  j2 = 1
  j3 = 1

  for row in points:
    #row = str(row) #将dict数据转换成字符
    #row = row.strip()
    #row = json.dumps(row) #将dict数据转换成json数据
    if len(row) > 0:
      file_points.write(str(row) + '\n')
      ipadd = ''

      # 类型Shadowsocks
      if row['configType']==3:
        d_ip = row['address']
        d_port = row['port']
        d_password = row['id']
        d_method = row['security']
        d_obfs = row['network']
        d_obfs_uri = row['path']
        d_obfs_host = row['requestHost']

        ip = re.search(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', d_ip)
        if ip is not None:
          ip = ip.group(0)
          #print (ip)
          reader = geoip2.database.Reader('GeoLite2-Country.mmdb')
          response = reader.country(ip)
          #ipadd = response.country.iso_code
          ipadd = response.country.names['zh-CN']
          #print (ipadd)
          if i<10:
            rownew = 'shadowsocks='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', method=aes-256-gcm, fast-open=false, udp-relay=false, tag=SS-'+str(ipadd)+'00'+str(i)
          elif i<100:
            rownew = 'shadowsocks='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', method=aes-256-gcm, fast-open=false, udp-relay=false, tag=SS-'+str(ipadd)+'0'+str(i)
          else:
            rownew = 'shadowsocks='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', method=aes-256-gcm, fast-open=false, udp-relay=false, tag=SS-'+str(ipadd)+str(i)
        else:
          if i<10:
            rownew = 'shadowsocks='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', method=aes-256-gcm, fast-open=false, udp-relay=false, tag=SS-'+'【Country】'+'00'+str(i)
            print ('⚠【Shadowsocks】 The No.'+'00'+ str(i) +' SS server is NOT IP. Please Search Manually.')
          elif i<100:
            rownew = 'shadowsocks='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', method=aes-256-gcm, fast-open=false, udp-relay=false, tag=SS-'+'【Country】'+'0'+str(i)
            print ('⚠【Shadowsocks】 The No.'+'0'+ str(i) +' SS server is NOT IP. Please Search Manually.')
          else:
            rownew = 'shadowsocks='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', method=aes-256-gcm, fast-open=false, udp-relay=false, tag=SS-'+'【Country】'+str(i)
            print ('⚠【Shadowsocks】 The No.'+ str(i) +' SS server is NOT IP. Please Search Manually.')
        pointsss.append(rownew)
        i += 1

      # 类型Vmess
      if row['configType']==1:
        d_ip = row['address']
        d_port = row['port']
        d_password = row['id']
        d_method = row['security']
        d_obfs = row['network']
        d_obfs_uri = row['path']
        d_obfs_host = row['requestHost']

        ip = re.search(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', d_ip)
        if ip is not None:
          ip = ip.group(0)
          #print (ip)
          reader = geoip2.database.Reader('GeoLite2-Country.mmdb')
          try:
            response = reader.country(ip)
            #ipadd = response.country.iso_code
            ipadd = response.country.names['zh-CN']
            #print (ipadd)
          except:
            print('⚠ '+ip+' Not Found In Database. Set 美国 as default.')
            #ipadd = '【Country】'
            ipadd = '美国'
            pass
        else:
          print('⚠ Found Vmess server is Not IP. Set 美国 as default.')
          #ipadd = '【Country】'
          ipadd = '美国'

        if d_port==443:
          if j1<10:
            rownew = 'vmess='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', method=aes-128-gcm, fast-open=false, udp-relay=false, tls13=true, obfs=wss, obfs-host='+str(d_obfs_host)+', obfs-uri='+str(d_obfs_uri)+', tag=VM(wss)-'+str(ipadd)+'00'+str(j1)
          elif j1<100:
            rownew = 'vmess='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', method=aes-128-gcm, fast-open=false, udp-relay=false, tls13=true, obfs=wss, obfs-host='+str(d_obfs_host)+', obfs-uri='+str(d_obfs_uri)+', tag=VM(wss)-'+str(ipadd)+'0'+str(j1)
          else:
            rownew = 'vmess='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', method=aes-128-gcm, fast-open=false, udp-relay=false, tls13=true, obfs=wss, obfs-host='+str(d_obfs_host)+', obfs-uri='+str(d_obfs_uri)+', tag=VM(wss)-'+str(ipadd)+str(j1)
          pointsvm1.append(rownew)
          j1 += 1
        else:
          if j2<10:
            rownew = 'vmess='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', method=aes-128-gcm, fast-open=false, udp-relay=false, obfs=ws, obfs-host='+str(d_obfs_host)+', obfs-uri='+str(d_obfs_uri)+', tag=VM-'+str(ipadd)+'00'+str(j2)
          elif j2<100:
            rownew = 'vmess='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', method=aes-128-gcm, fast-open=false, udp-relay=false, obfs=ws, obfs-host='+str(d_obfs_host)+', obfs-uri='+str(d_obfs_uri)+', tag=VM-'+str(ipadd)+'0'+str(j2)
          else:
            rownew = 'vmess='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', method=aes-128-gcm, fast-open=false, udp-relay=false, obfs=ws, obfs-host='+str(d_obfs_host)+', obfs-uri='+str(d_obfs_uri)+', tag=VM-'+str(ipadd)+str(j2)
          pointsvm2.append(rownew)
          j2 += 1

      # 类型Trojan
      if row['configType']==6:
        d_ip = row['address']
        d_port = row['port']
        d_password = row['id']
        d_method = row['security']
        d_obfs = row['network']
        d_obfs_uri = row['path']
        d_obfs_host = row['requestHost']

        ip = re.search(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', d_ip)
        if ip is not None:
          ip = ip.group(0)
          #print (ip)
          reader = geoip2.database.Reader('GeoLite2-Country.mmdb')
          try:
            response = reader.country(ip)
            #ipadd = response.country.iso_code
            ipadd = response.country.names['zh-CN']
            #print (ipadd)
          except:
            print('⚠ '+ip+' Not Found In Database. Set 美国 as default.')
            #ipadd = '【Country】'
            ipadd = '美国'
            pass
        else:
          print('⚠ Found No.'+str(j3)+' Trojan server is Not IP. Set 美国 as default.')
          #ipadd = '【Country】'
          ipadd = '美国'

        if j3<10:
          rownew = 'trojan='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', over-tls=true, tls-verification=true, tls13=true, tls-host='+str(d_obfs_host)+', fast-open=false, udp-relay=false, tag=Trojan-'+str(ipadd)+'00'+str(j3)
        elif j3<100:
          rownew = 'trojan='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', over-tls=true, tls-verification=true, tls13=true, tls-host='+str(d_obfs_host)+', fast-open=false, udp-relay=false, tag=Trojan-'+str(ipadd)+'0'+str(j3)
        else:
          rownew = 'trojan='+str(d_ip)+':'+str(d_port)+', password='+str(d_password)+', over-tls=true, tls-verification=true, tls13=true, tls-host='+str(d_obfs_host)+', fast-open=false, udp-relay=false, tag=Trojan-'+str(ipadd)+str(j3)

        pointstj.append(rownew)
        i += 1

  # 排序
  #pointsss.sort()
  #pointsvm1.sort()
  #pointsvm2.sort()
  #pointstj.sort()

  # 写入
  i = i-1
  j1 = j1-1
  j2 = j2-1
  j3 = j3-1
  file_pointsfss.write('# v2rayN SS Servers refresh time: ' + time.strftime("%Y-%m-%d %H:%M:%S") + '  |  Servers Number：' + str(i) + '\n\n')
  file_pointsfv1.write('# v2rayN Vmess(wss) Servers refresh time: ' + time.strftime("%Y-%m-%d %H:%M:%S") +  '  |  Servers Number：' + str(j1) + '\n\n')
  file_pointsfv2.write('# v2rayN Vmess Servers refresh time: ' + time.strftime("%Y-%m-%d %H:%M:%S") +  '  |  Servers Number：' + str(j2) + '\n\n')
  file_pointsftj.write('# v2rayN Trojan Servers refresh time: ' + time.strftime("%Y-%m-%d %H:%M:%S") +  '  |  Servers Number：' + str(j3) + '\n\n')
  for row1 in pointsss:
    file_pointsfss.write(row1 + '\n')
  for row2 in pointsvm1:
    file_pointsfv1.write(row2 + '\n')
  for row3 in pointsvm2:
    file_pointsfv2.write(row3 + '\n')
  for row4 in pointstj:
    file_pointsftj.write(row4 + '\n')

  file_points.close()
  file_pointsfss.close()
  file_pointsfv1.close()
  file_pointsfv2.close()
  file_pointsftj.close()

  print('Servers Fromed & File Writed.\n')
  return (i,j1,j2,j3)

def genClash():
  servers_url = [
    'https://www.githubip.xyz/Alvin9999/pac2/master/clash/1/config.yaml',
#    'https://ghproxy.com/https://raw.githubusercontent.com/Alvin9999/pac2/master/clash/1/config.yaml',
    'https://www.githubip.xyz/Alvin9999/pac2/master/clash/config.yaml',
#    'https://ghproxy.com/https://raw.githubusercontent.com/Alvin9999/pac2/master/clash/config.yaml',
    'https://www.githubip.xyz/Alvin9999/pac2/master/clash/13/config.yaml',
#    'https://ghproxy.com/https://raw.githubusercontent.com/Alvin9999/pac2/master/clash/13/config.yaml',
    'https://www.githubip.xyz/Alvin9999/pac2/master/clash/2/config.yaml',
#    'https://ghproxy.com/https://raw.githubusercontent.com/Alvin9999/pac2/master/clash/2/config.yaml',
    'https://www.githubip.xyz/Alvin9999/pac2/master/clash/15/config.yaml',
#    'https://ghproxy.com/https://raw.githubusercontent.com/Alvin9999/pac2/master/clash/15/config.yaml',
    'https://www.githubip.xyz/Alvin9999/pac2/master/clash/3/config.yaml',
#    'https://ghproxy.com/https://raw.githubusercontent.com/Alvin9999/pac2/master/clash/3/config.yaml',
  ]

  try:
    if sys.version_info.major == 3:
      file_points = open('Output/clash-servers.tmp', 'w', encoding='utf-8')
      file_pointsfss = open('Output/clash-ss.txt', 'w', encoding='utf-8')
      file_pointsfv1 = open('Output/clash-vmess(wss).txt', 'w', encoding='utf-8')
      file_pointsfv2 = open('Output/clash-vmess.txt', 'w', encoding='utf-8')
      file_pointsftj = open('Output/clash-trojan.txt', 'w', encoding='utf-8')
    else:
      file_points = open('Output/clash-servers.tmp', 'w')
      file_pointsfss = open('Output/clash-ss.txt', 'w')
      file_pointsfv1 = open('Output/clash-vmess(wss).txt', 'w')
      file_pointsfv2 = open('Output/clash-vmess.txt', 'w')
      file_pointsftj = open('Output/clash-trojan.txt', 'w')
  except:
    pass

  file_points.write('# Clash Servers tempfile refresh time: ' + time.strftime("%Y-%m-%d %H:%M:%S") + '\n\n')

  # 获取网页内容
  print('>>>>       Getting Clash servers list...      <<<<')
  servers = get_server_clash(servers_url)

  # 清洗：提取所有{}内的内容
  #points = re.findall(r'(?<=\{)[^}]*(?=\})', str(servers))
  points = re.findall(r'(?<=\{).*(?=\})', str(servers))

  # 初步去重复、转换成列表
  points = list(set(points))
  # points2.sort(key=points.index) #保留原列表元素排序

  # 深度去重复
  points = removeduplicate_clash(points)

  pointsss = []
  pointsvm1 = []
  pointsvm2 = []
  pointstj = []

  # 从10开始编号，保留前10个给v2rayN
  i = 11
  j1 = 11
  j2 = 11
  j3 = 11

  for row in points:
    row = row.strip()
    if len(row) > 0:
      file_points.write(row + '\n')
      ipadd = ''

      # 类型Shadowsocks
      if "type: ss" in row and "aes-256-gcm" in row:
        row = re.sub(r'(\bname.*server\b): ','shadowsocks=',row)
        row = re.sub(', port: ',':',row)
        row = re.sub(', type: ss','',row)
        row = re.sub(', cipher: ',', method=',row)
        row = re.sub('password: ','password=',row)

        ip = re.search(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', row)
        if ip is not None:
          ip = ip.group(0)
          #print (ip)
          reader = geoip2.database.Reader('GeoLite2-Country.mmdb')
          response = reader.country(ip)
          #ipadd = response.country.iso_code
          ipadd = response.country.names['zh-CN']
          #print (ipadd)
          if i<100:
            row = row + ', fast-open=false, udp-relay=false, tag=SS-'+str(ipadd)+'0'+str(i)
          else:
            row = row + ', fast-open=false, udp-relay=false, tag=SS-'+str(ipadd)+str(i)
        else:
          if i<100:
            row = row + ', fast-open=false, udp-relay=false, tag=SS-'+'【Country】'+'0'+str(i) 
            print ('⚠【Shadowsocks】 The No.'+'0'+ str(i) +' SS server is NOT IP. Please Search Manually.')
          else:
            row = row + ', fast-open=false, udp-relay=false, tag=SS-'+'【Country】'+str(i) 
            print ('⚠【Shadowsocks】 The No.'+ str(i) +' SS server is NOT IP. Please Search Manually.')
        pointsss.append(row)
        i += 1

      # 类型Vmess
      if "type: vmess" in row:
        if "🇺🇸"or"洛杉矶" in row:
          ipadd = '美国'
        if "🇫🇷" in row:
          ipadd = '法国'
        row = re.sub(r'(\bname.*server\b): ','vmess=',row)
        row = re.sub(', port: ',':',row)
        row = re.sub(', type: vmess','',row)
        row = re.sub(', uuid: ',', password=',row)
        row = re.sub(r', alterId: ([0-9]+)','',row)
        row = re.sub(', cipher: auto','',row)
        row = re.sub(', tls: true','',row)
        row = re.sub(', tls: false','',row)
        row = re.sub(', network: ',', obfs=',row)
        row = re.sub(', ws-opts: {path: ',', obfs-uri=',row)
        row = re.sub(', headers: {Host: ',', obfs-host=',row)
        row = re.sub('}','',row)

#        ip = re.search(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', row)
#        if ip is not None:
#          ip = ip.group(0)
#          #print (ip)
#          reader = geoip2.database.Reader('GeoLite2-Country.mmdb')
#          try:
#            response = reader.country(ip)
#            #ipadd = response.country.iso_code
#            ipadd = response.country.names['zh-CN']
#            #print (ipadd)
#          except:
#            print('⚠ '+ip+' Not Found In Database. Set 美国 as default.')
#            #ipadd = '【Country】'
#            ipadd = '美国'
#            pass
#        else:
#          print('⚠ Found Vmess server is Not IP. Set 美国 as default.')
#          #ipadd = '【Country】'
#          ipadd = '美国'

        if ":443, " in row:
          row = re.sub('obfs=ws','obfs=wss',row)
          if j1<100:
            row = row + ', method=aes-128-gcm, tls13=true, fast-open=false, udp-relay=false, tag=VM(wss)-'+str(ipadd)+'0'+str(j1)
          else:
            row = row + ', method=aes-128-gcm, tls13=true, fast-open=false, udp-relay=false, tag=VM(wss)-'+str(ipadd)+str(j1)
          pointsvm1.append(row)
          j1 += 1
        else:
          if j2<100:
            row = row + ', method=aes-128-gcm, fast-open=false, udp-relay=false, tag=VM-'+str(ipadd)+'0'+str(j2)
          else:
            row = row + ', method=aes-128-gcm, fast-open=false, udp-relay=false, tag=VM-'+str(ipadd)+str(j2)
          pointsvm2.append(row)
          j2 += 1

      # 类型Trojan
      if "type: trojan" in row:
        row = re.sub(r'(\bname.*server\b): ','trojan=',row)
        row = re.sub(', port: ',':',row)
        row = re.sub(', type: trojan','',row)
        row = re.sub(', password: ',', password=',row)
        row = re.sub(', sni: ',', tls-host=',row)

        ip = re.search(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', row)
        if ip is not None:
          ip = ip.group(0)
          #print (ip)
          reader = geoip2.database.Reader('GeoLite2-Country.mmdb')
          try:
            response = reader.country(ip)
            #ipadd = response.country.iso_code
            ipadd = response.country.names['zh-CN']
            #print (ipadd)
          except:
            print('⚠ '+ip+' Not Found In Database. Set 美国 as default.')
            #ipadd = '【Country】'
            ipadd = '美国'
            pass
        else:
          print('⚠ Found No.'+str(j3)+' Trojan server is Not IP. Set 美国 as default.')
          #ipadd = '【Country】'
          ipadd = '美国'

        if "skip-cert-verify: true" in row:  #抛弃无证书验证的节点
          pass
          #row = re.sub(', skip-cert-verify: true','',row)
          #if j3<100:
          #  row = row+', over-tls=true, tls-verification=false, fast-open=true, udp-relay=true, tag=Trojan-'+str(ipadd)+'0'+str(j3)+'🕳️'
          #else:
          #  row = row+', over-tls=true, tls-verification=false, fast-open=true, udp-relay=true, tag=Trojan-'+str(ipadd)+str(j3)+'🕳️'
          #pointstj.append(row)
          #j3 += 1
        else:
          if j3<100:
            row = row+', over-tls=true, tls-verification=true, fast-open=true, udp-relay=true, tag=Trojan-'+str(ipadd)+'0'+str(j3)
          else:
            row = row+ ', over-tls=true, tls-verification=true, fast-open=true, udp-relay=true, tag=Trojan-'+str(ipadd)+str(j3)
          pointstj.append(row)
          j3 += 1

  # 排序
  #pointsss.sort()
  #pointsvm1.sort()
  #pointsvm2.sort()
  #pointstj.sort()

  # 写入
  i=i-11
  j1=j1-11
  j2=j2-11
  j3=j3-11
  file_pointsfss.write('# Clash SS Servers refresh time: ' + time.strftime("%Y-%m-%d %H:%M:%S") + '  |  Servers Number：' + str(i) + '\n\n')
  file_pointsfv1.write('# Clash Vmess(wss) Servers refresh time: ' + time.strftime("%Y-%m-%d %H:%M:%S") +  '  |  Servers Number：' + str(j1) + '\n\n')
  file_pointsfv2.write('# Clash Vmess Servers refresh time: ' + time.strftime("%Y-%m-%d %H:%M:%S") +  '  |  Servers Number：' + str(j2) + '\n\n')
  file_pointsftj.write('# Clash Trojan Servers refresh time: ' + time.strftime("%Y-%m-%d %H:%M:%S") +  '  |  Servers Number：' + str(j3) + '\n\n')
  for row1 in pointsss:
    file_pointsfss.write(row1 + '\n')
  for row2 in pointsvm1:
    file_pointsfv1.write(row2 + '\n')
  for row3 in pointsvm2:
    file_pointsfv2.write(row3 + '\n')
  for row4 in pointstj:
    file_pointsftj.write(row4 + '\n')

  file_points.close()
  file_pointsfss.close()
  file_pointsfv1.close()
  file_pointsfv2.close()
  file_pointsftj.close()

  print('Servers Fromed & File Writed.\n')
  return (i,j1,j2,j3)


# 主函数
def main():

  path = os.getcwd()
  #path1 = path + '\\' + 'tmp'
  path2 = path + '\\' + 'Output'
  #if not os.path.exists(path1):
    #os.makedirs(path1)
  if not os.path.exists(path2):
    os.makedirs(path2)

  (Vi,Vj1,Vj2,Vj3) = genV2rayN() # 抓取 V2rayN 配置并传回服务器数量
  (Ci,Cj1,Cj2,Cj3) = genClash() # 抓取 Clash 配置并传回服务器数量

  # 合并文件
  if os.path.exists('Output/clash-ss.txt'):
    f1 = open('Output/clash-ss.txt', encoding='utf-8').read()  # 将打开的文件内容保存到变量f1
    log1 = open('Output/v2rayN-ss.txt', mode='a+', encoding='utf-8')  # 以追加模式打开文件，如果该文件不存在则创建。
    log1.write('\n'+f1)  # 将f1写入到log1，数据临时存储到缓冲区。
    log1.write('\nshadowsocks=127.0.0.1:443, method=aes-256-gcm, password=pwd, fast-open=false, udp-relay=false, tag=刷新: ' + time.strftime("%Y-%m-%d %H:%M:%S") + '\n\n')
    #节点个数：str(Vi+Ci)
    log1.close() # 将log1关闭。只有使用 close() 函数关闭文件时，才会将python缓冲区中的数据真正写入文件中。
    print('SS Servers files merged')
    time.sleep(1)
  if os.path.exists('Output/clash-vmess(wss).txt'):
    f2 = open('Output/clash-vmess(wss).txt', encoding='utf-8').read()
    log2 = open('Output/v2rayN-vmess(wss).txt', mode='a+', encoding='utf-8')
    log2.write('\n'+f2)
    log2.write('\nvmess=127.0.0.1:443, password=c248b9d5-91e9-421b-8135-c284c320b464, obfs=wss, obfs-uri=/ws, method=aes-128-gcm, tls13=true, fast-open=false, udp-relay=false, tag=刷新: ' + time.strftime("%Y-%m-%d %H:%M:%S") + '\n\n')
    #节点个数：str(Vj1+Cj1)
    log2.close()
    print('Vmess(wss) Servers files merged')
    time.sleep(1)
  if os.path.exists('Output/clash-vmess.txt'):
    f3 = open('Output/clash-vmess.txt', encoding='utf-8').read()
    log3 = open('Output/v2rayN-vmess.txt', mode='a+', encoding='utf-8')
    log3.write('\n'+f3)
    log3.write('\nvmess=127.0.0.1:80, password=c248b9d5-91e9-421b-8135-c284c320b464, method=none, fast-open=false, udp-relay=false, tag=刷新: ' + time.strftime("%Y-%m-%d %H:%M:%S") + '\n\n')
    #节点个数：str(Vj2+Cj2)
    log3.close()
    print('Vmess Servers files merged')
    time.sleep(1)
  if os.path.exists('Output/clash-trojan.txt'):
    f4 = open('Output/clash-trojan.txt', encoding='utf-8').read()
    log4 = open('Output/v2rayN-trojan.txt', mode='a+', encoding='utf-8')
    log4.write('\n'+f4)
    log4.write('\ntrojan=127.0.0.1:443, password=pwd, over-tls=true, tls-verification=true, fast-open=false, udp-relay=false, tag=刷新: ' + time.strftime("%Y-%m-%d %H:%M:%S") + '\n\n')
    #节点个数：str(Vj3+Cj3)
    log4.close()
    print('Trojan Servers files merged')

  # 复制到同步目录
  if os.path.exists('Output/v2rayN-ss.txt'):
    shutil.copy('Output/v2rayN-ss.txt', '../../sub/ss.txt')
    shutil.copy('Output/v2rayN-vmess(wss).txt', '../../sub/vmess-wss.txt')
    shutil.copy('Output/v2rayN-vmess.txt', '../../sub/vmess.txt')
    shutil.copy('Output/v2rayN-trojan.txt', '../../sub/trojan.txt')
    
    print('\nAll done!\nNow you need edit these files to sure they are right.')
  else:
    print('ERROR：NO def Generated！Check this py file!')


if __name__ == '__main__':
  main()




