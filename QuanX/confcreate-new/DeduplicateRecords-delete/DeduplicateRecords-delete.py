﻿#!/usr/bin/env python3
# coding=utf-8
#

# 使用方法：
#
# 把要去重的文件和对比文件放到'IN'文件夹下
#

import re
import codecs
import shutil

file3 = codecs.open('IN/ADB+_anti-AD.list', 'r', 'utf-8')  # 需要去重修改的源文件
file1 = codecs.open('IN/ADB_anti-AD.list', 'r', 'utf-8')  # 对比文件
file2 = codecs.open('ADB+_anti-AD_new.txt', 'w', 'utf-8')  # 去重后的新文件

def finddup(line):
  line0 = line
  ifstr = []
  file1.seek(0)
  for row in file1.readlines():
    row = row.strip()
    #print(line0)
    #print(row)
    ifstr = re.match(line0,row)
    #print(ifstr)
    if ifstr:
      break
  return ifstr


def main():
  i=0
  for line1 in file3.readlines():
    line1 = line1.strip()
    if len(line1)>0: 
      if line1.startswith('#') or line1.startswith('HOST-KEYWORD,'):
        file2.write(line1 + '\n')
      elif finddup(line1):
        print(line1)
        line1 = str('#--#') + line1
        file2.write(line1 + '\n')
        i += 1
      else:
        file2.write(line1 + '\n')
    else:
      file2.write('\n')

  file1.close()
  file2.close()
  file3.close()

  ## 复制到同步目录
  shutil.copy ('ADB+_anti-AD_new.txt', 'ADB+_anti-AD_new.list')

  print('All Done.'+'\n'+'Find '+str(i)+' host duplicated')


if __name__ == '__main__':
  main()

