if ($response.statusCode != 200) {
  $done(null);
}

var country0 = "未知国家";
var city0 = "未知城市";
var isp0 = "未知服务商";

function Country_ValidCheck(para) {
  if(para) {
    para = para.replace(/德意志联邦共和国/g,'德国');
    return para;
  } else
  { return country0 }
}

function City_ValidCheck(para) {
  if(para) {
    para = para.replace(/新加坡|香港/g,'');
    return para;
  } else
  { return city0 }
}

function ISP_ValidCheck(isp,org) {
  if(isp){
    isp = isp.replace(/.*Amazon.*/g,'Amazon AWS');
    isp = isp.replace(/.*Microsoft.*/g,'Microsoft Azure');
    isp = isp.replace(/.*Tencent.*/g,'腾讯云');
    isp = isp.replace(/.*Telekom.*/g,'Malaysia TMnet');
    isp = isp.replace(/.*Thien.*/g,'Thien Quang');
    isp = isp.replace(/.*HKT.*/g,'香港电讯(HKT)');
    isp = isp.replace(/HongKong|Hongkong|Hong\ Kong|Hong\ kong/g,'HK');
    isp = isp.replace(/Network|Networks|network|networks/g,'NWK');
    isp = isp.replace(/\ Technologies|\ Technology|\ Company/g,' ');
    isp = isp.replace(/Managed\ by|Proxy\ Registration|\ Services|\ (L|l)imited|\ Corporation|\ Communication(s)?|\ Solutions|\ Information|\ Center|\ Internet|\ PTE\.|\ Pte\ Ltd|\ Co\.|\ L(t|T)(d|D)(\.)?|\ Co\ Ltd|\ LLC|\ I(n|N)(c|C)(\.)?|"/g,'');
    isp = isp.replaceAll(',','');
    return isp;
  } else if(org)
  {
    isp = org;
    isp = isp.replace(/.*Amazon.*/g,'Amazon AWS');
    isp = isp.replace(/.*Microsoft.*/g,'Microsoft Azure');
    isp = isp.replace(/.*Tencent.*/g,'腾讯云');
    isp = isp.replace(/.*Telekom.*/g,'Malaysia TMnet');
    isp = isp.replace(/.*Thien.*/g,'Thien Quang');
    isp = isp.replace(/.*HKT.*/g,'香港电讯(HKT)');
    isp = isp.replace(/HongKong|Hongkong|Hong\ Kong|Hong\ kong/g,'HK');
    isp = isp.replace(/Network|Networks|network|networks/g,'NWK');
    isp = isp.replace(/\ Technologies|\ Technology|\ Company/g,' ');
    isp = isp.replace(/Managed\ by|Proxy\ Registration|\ Services|\ (L|l)imited|\ Corporation|\ Communication(s)?|\ Solutions|\ Information|\ Center|\ Internet|\ PTE\.|\ Pte\ Ltd|\ Co\.|\ L(t|T)(d|D)(\.)?|\ Co\ Ltd|\ LLC|\ I(n|N)(c|C)(\.)?|"/g,'');
    isp = isp.replaceAll(',','');
    return isp;
  } else
  { return isp0 }
}

//function Area_check(para) {
//  if(para=="中华民国"){
//  return "台湾"
//  } else
//  {
//  return para
//  }
//}

var flags = new Map([["AC","🇦🇨"],["AD","🇦🇩"],["AE","🇦🇪"],["AF","🇦🇫"],["AG","🇦🇬"],["AI","🇦🇮"],["AL","🇦🇱"],["AM","🇦🇲"],["AO","🇦🇴"],["AQ","🇦🇶"],["AR","🇦🇷"],["AS","🇦🇸"],["AT","🇦🇹"],["AU","🇦🇺"],["AW","🇦🇼"],["AX","🇦🇽"],["AZ","🇦🇿"],["BA","🇧🇦"],["BB","🇧🇧"],["BD","🇧🇩"],["BE","🇧🇪"],["BF","🇧🇫"],["BG","🇧🇬"],["BH","🇧🇭"],["BI","🇧🇮"],["BJ","🇧🇯"],["BL","🇧🇱"],["BM","🇧🇲"],["BN","🇧🇳"],["BO","🇧🇴"],["BQ","🇧🇶"],["BR","🇧🇷"],["BS","🇧🇸"],["BT","🇧🇹"],["BV","🇧🇻"],["BW","🇧🇼"],["BY","🇧🇾"],["BZ","🇧🇿"],["CA","🇨🇦"],["CC","🇨🇨"],["CD","🇨🇩"],["CF","🇨🇫"],["CG","🇨🇬"],["CH","🇨🇭"],["CI","🇨🇮"],["CK","🇨🇰"],["CL","🇨🇱"],["CM","🇨🇲"],["CN","🇨🇳"],["CO","🇨🇴"],["CP","🇨🇵"],["CR","🇨🇷"],["CU","🇨🇺"],["CV","🇨🇻"],["CW","🇨🇼"],["CX","🇨🇽"],["CY","🇨🇾"],["CZ","🇨🇿"],["DE","🇩🇪"],["DG","🇩🇬"],["DJ","🇩🇯"],["DK","🇩🇰"],["DM","🇩🇲"],["DO","🇩🇴"],["DZ","🇩🇿"],["EA","🇪🇦"],["EC","🇪🇨"],["EE","🇪🇪"],["EG","🇪🇬"],["EH","🇪🇭"],["ER","🇪🇷"],["ES","🇪🇸"],["ET","🇪🇹"],["EU","🇪🇺"],["FI","🇫🇮"],["FJ","🇫🇯"],["FK","🇫🇰"],["FM","🇫🇲"],["FO","🇫🇴"],["FR","🇫🇷"],["GA","🇬🇦"],["GB","🇬🇧"],["GD","🇬🇩"],["GE","🇬🇪"],["GF","🇬🇫"],["GG","🇬🇬"],["GH","🇬🇭"],["GI","🇬🇮"],["GL","🇬🇱"],["GM","🇬🇲"],["GN","🇬🇳"],["GP","🇬🇵"],["GQ","🇬🇶"],["GR","🇬🇷"],["GS","🇬🇸"],["GT","🇬🇹"],["GU","🇬🇺"],["GW","🇬🇼"],["GY","🇬🇾"],["HK","🇭🇰"],["HM","🇭🇲"],["HN","🇭🇳"],["HR","🇭🇷"],["HT","🇭🇹"],["HU","🇭🇺"],["IC","🇮🇨"],["ID","🇮🇩"],["IE","🇮🇪"],["IL","🇮🇱"],["IM","🇮🇲"],["IN","🇮🇳"],["IO","🇮🇴"],["IQ","🇮🇶"],["IR","🇮🇷"],["IS","🇮🇸"],["IT","🇮🇹"],["JE","🇯🇪"],["JM","🇯🇲"],["JO","🇯🇴"],["JP","🇯🇵"],["KE","🇰🇪"],["KG","🇰🇬"],["KH","🇰🇭"],["KI","🇰🇮"],["KM","🇰🇲"],["KN","🇰🇳"],["KP","🇰🇵"],["KR","🇰🇷"],["KW","🇰🇼"],["KY","🇰🇾"],["KZ","🇰🇿"],["LA","🇱🇦"],["LB","🇱🇧"],["LC","🇱🇨"],["LI","🇱🇮"],["LK","🇱🇰"],["LR","🇱🇷"],["LS","🇱🇸"],["LT","🇱🇹"],["LU","🇱🇺"],["LV","🇱🇻"],["LY","🇱🇾"],["MA","🇲🇦"],["MC","🇲🇨"],["MD","🇲🇩"],["ME","🇲🇪"],["MF","🇲🇫"],["MG","🇲🇬"],["MH","🇲🇭"],["MK","🇲🇰"],["ML","🇲🇱"],["MM","🇲🇲"],["MN","🇲🇳"],["MO","🇲🇴"],["MP","🇲🇵"],["MQ","🇲🇶"],["MR","🇲🇷"],["MS","🇲🇸"],["MT","🇲🇹"],["MU","🇲🇺"],["MV","🇲🇻"],["MW","🇲🇼"],["MX","🇲🇽"],["MY","🇲🇾"],["MZ","🇲🇿"],["NA","🇳🇦"],["NC","🇳🇨"],["NE","🇳🇪"],["NF","🇳🇫"],["NG","🇳🇬"],["NI","🇳🇮"],["NL","🇳🇱"],["NO","🇳🇴"],["NP","🇳🇵"],["NR","🇳🇷"],["NU","🇳🇺"],["NZ","🇳🇿"],["OM","🇴🇲"],["PA","🇵🇦"],["PE","🇵🇪"],["PF","🇵🇫"],["PG","🇵🇬"],["PH","🇵🇭"],["PK","🇵🇰"],["PL","🇵🇱"],["PM","🇵🇲"],["PN","🇵🇳"],["PR","🇵🇷"],["PS","🇵🇸"],["PT","🇵🇹"],["PW","🇵🇼"],["PY","🇵🇾"],["QA","🇶🇦"],["RE","🇷🇪"],["RO","🇷🇴"],["RS","🇷🇸"],["RU","🇷🇺"],["RW","🇷🇼"],["SA","🇸🇦"],["SB","🇸🇧"],["SC","🇸🇨"],["SD","🇸🇩"],["SE","🇸🇪"],["SG","🇸🇬"],["SH","🇸🇭"],["SI","🇸🇮"],["SJ","🇸🇯"],["SK","🇸🇰"],["SL","🇸🇱"],["SM","🇸🇲"],["SN","🇸🇳"],["SO","🇸🇴"],["SR","🇸🇷"],["SS","🇸🇸"],["ST","🇸🇹"],["SV","🇸🇻"],["SX","🇸🇽"],["SY","🇸🇾"],["SZ","🇸🇿"],["TA","🇹🇦"],["TC","🇹🇨"],["TD","🇹🇩"],["TF","🇹🇫"],["TG","🇹🇬"],["TH","🇹🇭"],["TJ","🇹🇯"],["TK","🇹🇰"],["TL","🇹🇱"],["TM","🇹🇲"],["TN","🇹🇳"],["TO","🇹🇴"],["TR","🇹🇷"],["TT","🇹🇹"],["TV","🇹🇻"],["TW","🇨🇳"],["TZ","🇹🇿"],["UA","🇺🇦"],["UG","🇺🇬"],["UK","🇬🇧"],["UM","🇺🇲"],["US","🇺🇸"],["UY","🇺🇾"],["UZ","🇺🇿"],["VA","🇻🇦"],["VC","🇻🇨"],["VE","🇻🇪"],["VG","🇻🇬"],["VI","🇻🇮"],["VN","🇻🇳"],["VU","🇻🇺"],["WF","🇼🇫"],["WS","🇼🇸"],["XK","🇽🇰"],["YE","🇾🇪"],["YT","🇾🇹"],["ZA","🇿🇦"],["ZM","🇿🇲"],["ZW","🇿🇼"]])
var body = $response.body;
var obj = JSON.parse(body);
var title =flags.get(obj['countryCode'])+' '+Country_ValidCheck(obj['country'])+' '+City_ValidCheck(obj['city']);  //Area_check(obj['country'])
var subtitle =ISP_ValidCheck(obj['isp'],obj['org'])+'｜'+obj['query'];
var ip = obj['query'];
var description ='------------------------------'+'\n'+'IP:'+ip+'\n'+'国家:'+obj['country']+'\n'+'地区:'+obj['regionName']+'\n'+'城市:'+obj['city']+'\n\n'+'网络服务商:'+obj['isp']+'\n'+'隶属机构:'+obj['org']+'\n'+'AS信息:'+obj['as']+'\n\n'+'时区:'+obj['timezone']+'\n'+'经纬度:'+obj['lat']+', '+obj['lon']+'\n'+'------------------------------';
$done({title, subtitle, ip, description});