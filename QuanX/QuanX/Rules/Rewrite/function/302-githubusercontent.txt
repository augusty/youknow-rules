# Github反代(https://cjh0613.com/githubproxy.html, https://doc.fastgit.org/zh-cn/)

# 无缓存
^https://raw.githubusercontent.com url 302 https://mirror.ghproxy.com/https://raw.githubusercontent.com
#^https://gist.github.com url 302 https://mirror.ghproxy.com/https://gist.github.com
#^https://gist.githubusercontent.com url 302 https://mirror.ghproxy.com/https://gist.github.com

hostname=raw.githubusercontent.com




