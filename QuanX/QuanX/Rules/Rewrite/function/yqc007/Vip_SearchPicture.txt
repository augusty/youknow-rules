# 脚本功能：搜图神器解锁会员
# 软件版本：1.3.3
# 下载地址：http://t.cn/A6o9s1WB
# 脚本作者：Hausd0rff
# 更新时间：2022-10-26

# > 搜图神器解锁会员
^https?:\/\/wallpaper\.soutushenqi\.com\/api\/v1\/account\/token$ url script-response-body https://www.gitlabip.xyz/yqc007/QuantumultX/master/SearchPictureCrack.js

hostname = wallpaper.soutushenqi.com