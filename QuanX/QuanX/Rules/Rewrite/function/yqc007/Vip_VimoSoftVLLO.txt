# 脚本功能：VLLO解锁订阅
# 软件版本：7.11.1
# 下载地址：http://t.cn/A6A8imIz
# 脚本作者：Hausd0rff
# 更新时间：2021.11.20

# VLLO解锁订阅
^https?:\/\/buy\.itunes\.apple\.com\/verifyReceipt$ url script-response-body https://www.gitlabip.xyz/yqc007/QuantumultX/master/VimoSoftVLLOProCrack.js

hostname = buy.itunes.apple.com