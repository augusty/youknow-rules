# 脚本功能：格志日记解锁永久高级版
# 软件版本：3.2.1
# 下载地址：http://t.cn/A6oDgPyl
# 脚本作者：Hausd0rff
# 更新时间：2022-11-13

# > 格志日记解锁永久高级版
^https?:\/\/diary-id\.sumi\.io\/api\/profile$ url script-response-body https://www.gitlabip.xyz/yqc007/QuantumultX/master/GridDiary2ProCrack.js

hostname = diary-id.sumi.io