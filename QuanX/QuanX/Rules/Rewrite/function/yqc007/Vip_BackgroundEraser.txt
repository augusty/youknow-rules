# 脚本功能：傲软抠图解锁会员
# 软件版本：1.5.1
# 下载地址：http://t.cn/A6xBOE5d
# 脚本作者：Hausd0rff
# 更新时间：2021-11-20

# > 傲软抠图解锁会员
^https?:\/\/gw\.aoscdn\.com\/base\/vip\/client\/authorizations$ url script-response-body https://www.gitlabip.xyz/yqc007/QuantumultX/master/BackgroundEraserProCrack.js

hostname = gw.aoscdn.com