# 脚本功能：TIDAL解锁HiFi Plus
# 软件版本：2.48.0【美区下载】
# 下载地址：http://t.cn/A662gqIO
# 脚本作者：Hausd0rff
# 更新时间：2022-08-05

# > TIDAL解锁HiFi Plus
^https?:\/\/api\.tidal\.com\/v1\/users\/\d+\/subscription.+ url script-response-body https://www.gitlabip.xyz/yqc007/QuantumultX/master/TIDALHiFiPlusCrack.js
^https?:\/\/api\.tidal\.com\/v1\/tracks/\d+\/playbackinfopostpaywall.+ url script-analyze-echo-response https://www.gitlabip.xyz/yqc007/QuantumultX/master/TidalHiFiPlusCrack.js

hostname = api.tidal.com