# 脚本功能：91短视频解锁会员&金币视频
# 软件版本：4.6.0
# 下载地址：https://shrtm.nu/91sv
# 脚本作者：Hausd0rff
# 更新时间：2021-11-14


# > 91短视频解锁会员&金币视频
^https?:\/\/.+\.(my10api|(.*91.*))\.\w{3,4}(:\d{2,5})?\/api.php$ url script-response-body https://www.gitlabip.xyz/yqc007/QuantumultX/master/91SVCrack.js

hostname = *.91api*