# 脚本功能：鱿鱼视频解锁会员
# 软件版本：1.1.1
# 下载地址：https://shrtm.nu/yysp
# 脚本作者：Hausd0rff
# 更新时间：2022-01-04

# > 鱿鱼视频解锁会员
^https?:\/\/api\.youyu.*\/api\/account\/loginBy(Phone|Password)$ url script-response-body https://www.gitlabip.xyz/yqc007/QuantumultX/master/YYSPCrack.js

hostname = *.youyu*