# 脚本功能：艾薇啵啵解锁会员
# 网页版本：1.1.0
# 网站地址：https://shrtm.nu/avbb
# 脚本作者：Hausd0rff
# 更新时间：2022-01-26


# > 艾薇啵啵解锁会员
^https?:\/\/s\..*\.com\/s2\/L2d3L3VzZXItc2VydmVyL3dlYi9sb2dpbj url script-response-body https://www.gitlabip.xyz/yqc007/QuantumultX/master/AVBBCrack.js

hostname = s.*.com