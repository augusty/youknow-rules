# 脚本功能：汤头条解锁会员
# 软件版本：2.2.2
# 下载地址：https://shrtm.nu/ttsp
# 脚本作者：Hausd0rff
# 更新时间：2021-11-12

# > 汤头条解锁会员
^https?:\/\/(api\w{0,1}|ttt\w{0,1})\.tbrapi\.\w{3}:\d{4}\/api\.php\/api\/user\/userinfo url script-response-body https://www.gitlabip.xyz/yqc007/QuantumultX/master/TTSPCrack.js

hostname = *.tbrapi*