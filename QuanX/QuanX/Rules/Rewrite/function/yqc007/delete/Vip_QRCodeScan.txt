# 脚本功能：QrScanner解锁订阅
# 软件版本：5.4.0
# 下载地址：http://t.cn/A6JLAr5p
# 脚本作者：Hausd0rff
# 更新时间：2021-11-21

# > QrScanner解锁订阅
^https?:\/\/buy\.itunes\.apple\.com\/verifyReceipt$ url script-response-body https://www.gitlabip.xyz/yqc007/QuantumultX/master/QRCodeScanProCrack.js

hostname = buy.itunes.apple.com