# 脚本功能：NoMo Cam 解锁订阅
# 软件版本：1.5.131
# 下载地址：http://t.cn/A6xIUPmm
# 脚本作者：Hausd0rff
# 更新时间：2022-09-26

# > NoMo Cam 解锁订阅
^https?:\/\/nomo\.dafork\.com\/api\/v2\/iap\/ios_verify$ url script-request-body https://www.gitlabip.xyz/yqc007/QuantumultX/master/NoMoCamProCrack.js

hostname = nomo.dafork.com