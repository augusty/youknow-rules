# 脚本功能：Grammarly解锁订阅
# 软件版本：2.2.0
# 下载地址：http://t.cn/A66x3u4I
# 脚本作者：Hausd0rff
# 更新时间：2022-07-16

# > Grammarly解锁订阅
^https?:\/\/subscription\.grammarly\.com\/api\/v1\/subscription$ url script-response-body https://www.gitlabip.xyz/yqc007/QuantumultX/master/GrammarlyPremiumCrack.js

hostname = subscription.grammarly.com