# -*- coding: utf-8 -*-

import os

def all_path():

  path = os.getcwd()+"\\"
  
  for root, dirs, files in os.walk(path):  # 该文件夹下所有的文件（包括文件夹）
    for file in files:  # 遍历所有文件
      filename = os.path.splitext(file)[0]  # 文件名
      filetype = os.path.splitext(file)[1]  # 文件扩展名
      #print (filetype)
      filePath = os.path.join(root, file)


      filetypekey = ['.txt','.conf','.js']
      newstr1_1 = '-response https://www.gitlabip.xyz/'
      newstr2_1 = '-body https://www.gitlabip.xyz/'
      newstr3_1 = '-header https://www.gitlabip.xyz/'

      oldstr1_1 = '-response https://raw.githubusercontent.com/'
      oldstr2_1 = '-body https://raw.githubusercontent.com/'
      oldstr3_1 = '-header https://raw.githubusercontent.com/'


      if filetype in filetypekey:
        f1 = open(filePath,'r',encoding='utf-8');
        content = f1.read();
        
        if oldstr1_1 in content:
          f2 = open(root+'\\'+filename+'-new'+filetype,'w',encoding='utf-8');
          f2.write(content.replace(oldstr1_1,newstr1_1));
          f2.close()
          f1.close()
          # 删除原文件
          os.remove(filePath)
          os.rename(root+'\\'+filename+'-new'+filetype, filePath)

        if oldstr2_1 in content:
          f2 = open(root+'\\'+filename+'-new'+filetype,'w',encoding='utf-8');
          f2.write(content.replace(oldstr2_1,newstr2_1));
          f2.close()
          f1.close()
          # 删除原文件
          os.remove(filePath)
          os.rename(root+'\\'+filename+'-new'+filetype, filePath)

        if oldstr3_1 in content:
          f2 = open(root+'\\'+filename+'-new'+filetype,'w',encoding='utf-8');
          f2.write(content.replace(oldstr3_1,newstr3_1));
          f2.close()
          f1.close()
          # 删除原文件
          os.remove(filePath)
          os.rename(root+'\\'+filename+'-new'+filetype, filePath)


def main():
  all_path()
  print ('Done！')

if __name__ == '__main__':
  main()
