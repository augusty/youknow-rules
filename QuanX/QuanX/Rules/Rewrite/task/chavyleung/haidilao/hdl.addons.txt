#   【使用说明】
#   1. 启用脚本。
#   2. 打开 APP[海底捞](https://apps.apple.com/cn/app/%E6%B5%B7%E5%BA%95%E6%8D%9E/id553115181) 然后手动签到 1 次, 系统提示: `获取Cookie: 成功`。
#   3. 运行一次脚本, 如果提示重复签到, 那就算成功了!


^https:\/\/activity-1\.m\.duiba\.com\.cn\/signactivity\/doSign$ url script-request-body https://www.gitlabip.xyz/chavyleung/scripts/master/haidilao/hdl.js

hostname = activity-1.m.duiba.com.cn
